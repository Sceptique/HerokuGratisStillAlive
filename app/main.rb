class MainController < Nephos::Controller

  def root
    {
      json: {
        list: $dataset,
        add: '/add',
        rm: '/rm',
      }
    }
  end

  def add_url
    url = params[:url]
    if url
      Dataset << url
      return {plain: "#{url} added"}
    else
      return {plain: "url argument required"}
    end
  end

  def rm_url
    url = params[:url]
    if url
      Dataset.rm url
      return {plain: "#{url} removed"}
    else
      return {plain: "url argument required"}
    end
  end

end
